const { Gitlab, ProjectsBundle  } = require('@gitbeaker/node');
const { Octokit } = require("@octokit/rest");
const rimraf = require('rimraf');
const mkdirp = require('mkdirp');
const fs = require('fs');
require('dotenv').config();


const initGitlab = ()=>{
	const api = new Gitlab({
		token: process.env.GITLAB_TOKEN
	});
	return api;
}

const initGithub = ()=>{
	const octokit = new Octokit({
		auth: process.env.GITHUB_TOKEN,
	});	
	return octokit;
}


const listGitlabRepos = async (api)=>{
	console.log('Fetching Gitlab repo listing....');
	const repos_full = await api.Users.projects(process.env.GITLAB_ID);
	const repos = repos_full.map(repo=>(({ http_url_to_repo, name, description, visibility }) => ({ http_url_to_repo, name, description, visibility }))(repo));
	return repos;
}
const listGithubRepos = async (ocotokit)=>{
	const octokit = new Octokit({
		auth: process.env.GITHUB_TOKEN,
	});	
	console.log('Fetching Github repo listing....');
	let repos_full = [];
	let i = 0;
	while(true){
		const new_repos = await octokit.rest.repos.listForAuthenticatedUser({affiliation:'owner', page:i});
		repos_full = repos_full.concat(new_repos.data);
		if(!new_repos.data.length) break;
		i++;
	}
	const repos = repos_full.map(repo=>(({ name,clone_url }) => ({ name, clone_url }))(repo));
	return repos;
	//createForAuthenticatedUser
}

const processRepoLists = (gitlabRepos,githubRepos)=>{
	return gitlabRepos.map(repo=>{
		const githubRepo = githubRepos.find(gr=>gr.name.toLowerCase() === repo.name.toLowerCase().split(' ').join('-'));
		if(githubRepo){
			repo.githubRemote = githubRepo.clone_url;
		}
		return repo;
	});
}

const createGithubRepos = async (repoList,octokit) =>{
	console.log('Creating new Github repos....');
	return await Promise.all(repoList.map(async (repo,i)=>{
		if(repo.githubRemote) return repo;
		const options = {
			name:repo.name,
			description:repo.description,
			private:repo.visibility === 'public'?false:true
		}
		await hardWait(i*100);
		console.log(`Creating repo for ${repo.name}`);
		const newRepo = await octokit.rest.repos.createForAuthenticatedUser(options);
		repo.githubRemote = newRepo.clone_url;
		return repo;

	}));
}

const credify = (user,token,repo)=>`https://${user}:${token}@${repo.split('https://')[1]}`;

const hardWait = ms =>{
	return new Promise((resolve,reject)=>{
		setTimeout(()=>{resolve()},ms);
	});
}

const syncRepos = async createdRepos =>{
	console.log('Syncing repos....');
	await Promise.all(createdRepos.map(async (repo,i)=>{
		if(!repo.githubRemote) return repo;
		if(!repo.http_url_to_repo) return repo;
		rimraf.sync(repo.name.trim());
		mkdirp.sync(repo.name.trim());
		const git = require('simple-git/promise')(repo.name.trim());
		const gitlabRepo = credify(process.env.GITLAB_USER,process.env.GITLAB_TOKEN,repo.http_url_to_repo);
		const githubRepo = credify(process.env.GITHUB_USER,process.env.GITHUB_TOKEN,repo.githubRemote);
		//await git.init();
		await hardWait(i*300);
		console.log(`Mirroring repo ${repo.name}`);
		await git.mirror(gitlabRepo,'./.git');
		await git.addRemote('github',githubRepo);	
		console.log(`Pushing repo ${repo.name}`);
		await git.raw(['push','github','--mirror']);
		rimraf.sync(repo.name.trim());

	}));

}

const init = async ()=>{
	const gitlabApi = initGitlab();
	const githubApi = initGithub();
	const githubRepos = await listGithubRepos(githubApi);
	//return console.log(githubRepos);
	const gitlabRepos = await listGitlabRepos(gitlabApi);
	const repoList = processRepoLists(gitlabRepos,githubRepos);
	//return console.log(repoList);
	const createdRepos = await createGithubRepos(repoList,githubApi);
	await syncRepos(createdRepos);
	console.log('Repo sync complete!');
}
init();
